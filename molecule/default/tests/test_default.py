import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ssh_proxy')


def test_default(host):
    assert host.file('/home/srv_ansible/.ssh/authorized_keys').content_string == "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1h6s7GoBWbz3kjjQyejsQ+eFZfUMaOqQZuH4MC11d+xcfPZxSQOvSubkRPXUdkr9rJhirmBlanGmzp9w9tcCVxLTeKN0/5fMHo4/ck9/Cv6brnxmVnoqoyfFJR3p/WiN6LdONsgvajtssbMApBy6Ez3iUU4hbYws02YgscWMhn6oZlikbBMpxb1RMnTONra8WIcT/s7wqy37fwZPAXfq3HEL8aVqXfJ+faGGdxELVwbQdHX6CG4L9zaCoV/OZ2oo7MqybWKfd7/AbImhtcGMt7jVzPiiKYJ2wIbXxRgeymm1HOMdVHU6FsL3AkMZTl0piiXLcmyQTAWYW55CFUlUV awx_task@torn.tn.esss.lu.se\n"
