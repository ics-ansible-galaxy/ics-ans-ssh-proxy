ics-ans-ssh-proxy
===================

Ansible playbook to set up ssh jump hosts.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
